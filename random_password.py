#!/usr/bin/env python3

import random

import tkinter
from tkinter import ttk


class PasswordGenerator(tkinter.Tk):
    def init(self):
        title_label = tkinter.Label(self,
                                         text='Random Password Generator',
                                         font=('Helvetica Neue', 28, 'bold'))
        title_label.grid(row=0, columnspan=4, padx=5, pady=5)
        
        # Length Selection Box
        lengths = list(range(1, 19))
        self.length_var = tkinter.IntVar()
        
        length_label = tkinter.Label(self, text='Length')
        length_label.grid(row=1, column=1, sticky=tkinter.E)
        
        length_selector = ttk.Combobox(self,
                                       textvariable=self.length_var,
                                       values=lengths,
                                       justify='center')
        length_selector.grid(row=1, column=2, padx=7.5, sticky=tkinter.W)
        
        # Random Password Field
        self.password_field = tkinter.Entry(self, width=30)
        self.password_field.grid(row=2, columnspan=4, padx=5, pady=2.5)

        self.sample_set = list(r'ABCDEFGHIJKLMNOPQRSTUVWXYZ!$`\'" |\/'
                               r'abcdefghijklmnopqrstuvwxyz1234567890@-_')
        self.password_var = tkinter.StringVar()

        self.password_field.config(state=['readonly'])
        self.password_field.config(justify='center')
        self.password_field.config(textvariable=self.password_var)


    def run(self):
        self.init()

        generate_button = tkinter.Button(
            self,
            text='Generate',
            command=self.generate_random_password)
        generate_button.grid(row=3,
                             column=1,
                             padx=2.5,
                             pady=2.5,
                             sticky=tkinter.E)

        exit_button = tkinter.Button(self, text='Quit', command=self.destroy)
        exit_button.grid(row=3, column=2)

        self.mainloop()


    def generate_random_password(self):
        password = ''

        for _ in range(self.length_var.get()):
            password += random.choice(self.sample_set)

        self.password_var.set(password)


if __name__ == '__main__':
    app = PasswordGenerator(screenName='Random Password',
                            baseName='Random Password',
                            className='Random Password')
    app.run()