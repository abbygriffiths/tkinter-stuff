#!/usr/bin/env python3

import os

import tkinter
from tkinter import constants
from tkinter import ttk


class App(tkinter.Tk):
    def run(self):
        hello_label = tkinter.Label(self,
            text='The current app is running from {}'.format(__file__))
        hello_label.grid(row=0)
        
        cedille_label = tkinter.Label(self,
                                      text='This should be a cedilla: ç')
        cedille_label.grid(row=1)
        
        self.expand_button = tkinter.Button(self,
            text='Click me', command=self.button_clicked)
        self.expand_button.grid(row=2, columnspan=2)

        exit_button = ttk.Button(self, text='Exit', command=self.destroy)
        exit_button.grid(row=3)
        
        self.mainloop()


    def button_clicked(self):
        self.expand_button['text'] = '[{}]'.format(self.expand_button['text'])


if __name__ == '__main__':
    app = App()
    app.run()
