#!/usr/bin/env python3

import tkinter
from tkinter import ttk


class App(tkinter.Tk):
    def run(self):
        value = tkinter.DoubleVar()
        value.set(0)
        
        scale = ttk.Progressbar(self, orient=tkinter.HORIZONTAL,
                                length=200, variable=value, mode='determinate',
                                maximum=10, value=1)
        scale.pack()
        
        slider = ttk.Scale(self, orient=tkinter.HORIZONTAL, length=200,
                           from_=0, to=10, variable=value)
        slider.pack()
        
        self.mainloop()
        
        
if __name__ == '__main__':
    app = App()
    app.run()