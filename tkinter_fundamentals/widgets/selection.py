#!/usr/bin/env python3

import tkinter
from tkinter import ttk


class App(tkinter.Tk):
    def run(self):
        month = tkinter.StringVar()
        
        months = ('January', 'February', 'March', 'April',
                  'May', 'June','July', 'August',
                  'September','October', 'November', 'December')
        
        combo_box = ttk.Combobox(self, textvariable=month)        
        combo_box.pack()
        
        combo_box.configure(values=months)
        
        self.mainloop()
    
    
if __name__ == '__main__':
    app = App(screenName='Selections')
    app.run()