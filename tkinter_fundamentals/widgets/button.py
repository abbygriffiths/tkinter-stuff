#!/usr/bin/env python3

import tkinter
from tkinter import ttk


class App(tkinter.Tk):
    def run(self):
        button = ttk.Button(self, text='Click me!')
        button.config(command=self.button_clicked)
        button.pack()
        
        self.mainloop()


    def button_clicked(self, event=None):
        print('Button clicked!')


if __name__ == '__main__':
    app = App()
    app.run()
