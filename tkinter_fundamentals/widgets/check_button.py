#!/usr/bin/env python3

import tkinter
from tkinter import ttk


class App(tkinter.Tk):
    def run(self):
        def button_checked(self, event=None):
            pass
        
        
        check_button = ttk.Checkbutton(self, text='Spam?')
        check_button.pack()
        
        spam = tkinter.StringVar()
        spam.set('Spam!')
        
        check_button.config(variable=spam,
                            onvalue='Spam please!',
                            offvalue='No spam thanks.')
        
        self.mainloop()
        
        
if __name__ == '__main__':
    app = App()
    app.run()