#!/usr/bin/env python3

import tkinter


class App(tkinter.Tk):
    def main(self):
        label = tkinter.Label(self, text='Hello, Tkinter!')
        label.config(background='yellow', foreground='blue')
        
        label.config(font=('MonacoB2', 14, 'bold'))

        logo = tkinter.PhotoImage(file='/Users/abirbhavg/Movies/Programming/'
                                  'Python GUI Development/Exercise Files/'
                                  'Ch03/python_logo.gif')

        label.config(image=logo)
        label.pack()
        
        label.config(compound='left')

        self.mainloop()


if __name__ == '__main__':
    app = App(screenName='Hello', baseName='Hello', className='Hello')
    app.run()
