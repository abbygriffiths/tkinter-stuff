#!/usr/bin/env python3

import tkinter
from tkinter import constants


class App:
    def left_click(self, event):
        print("Left mouse button clicked!")


    def right_click(self, event):
        print("Right mouse button clicked!")

    
    def __init__(self):
        window = tkinter.Tk()
        
        frame = tkinter.Frame(window, width=300, height=250)
        
        frame.bind("<Button-1>", self.left_click)
        frame.bind("<Button-2>", self.right_click)
        
        frame.pack()
        window.mainloop()


if __name__ == '__main__':
    App()
