#!/usr/bin/env python3

import tkinter


class App:
    def __init__(self):
        def delete_graphics(event):
            canvas.delete(tkinter.ALL)
                
        
        window = tkinter.Tk()
        
        canvas = tkinter.Canvas(window, width=200, height=100)
        canvas.pack()
        
        black_line = canvas.create_line((0, 0), (200, 50))
        red_line = canvas.create_line((0, 100), (200, 50), fill='red')
        
        green_box = canvas.create_rectangle((25, 25), (75, 75), fill="green")
        
        delete_button = tkinter.Button(window, text="Delete graphics.")
        delete_button.bind("<Button-1>", delete_graphics)
        delete_button.pack()
        
        window.mainloop()
        
        
if __name__ == '__main__':
    App()