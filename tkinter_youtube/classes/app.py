#!/usr/bin/env python3

import tkinter
from tkinter import constants


class App():
    def __init__(self):
        window = tkinter.Tk()

        frame = tkinter.Frame(window)
        frame.pack()

        print_button = tkinter.Button(frame,
                                           text="Print message",
                                           command=self.print_message)
        print_button.grid(row=0, columnspan=2)

        exit_button = tkinter.Button(frame,
                                          text="Exit",
                                          command=frame.quit)
        exit_button.grid(row=1, columnspan=2)

        window.mainloop()


    def print_message(self):
        print("You clicked the button!")


if __name__ == '__main__':
    app = App()