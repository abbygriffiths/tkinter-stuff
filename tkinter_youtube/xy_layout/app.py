#!/usr/local/bin/python3

import tkinter
from tkinter import constants


class App:
	def print_name(self):
		print("Hi, I'm Abby!")
	
	
	def __init__(self):
		window = tkinter.Tk()

		name_label = tkinter.Label(window, text="Name")
		pass_label = tkinter.Label(window, text="Password")
		
		name_entry = tkinter.Entry(window)
		pass_entry = tkinter.Entry(window)
		
		name_label.grid(row=0, sticky=constants.E)
		name_entry.grid(row=0, column=1)
		
		pass_label.grid(row=1, sticky=constants.E)
		pass_entry.grid(row=1, column=1)
		
		logged_in_checkbox = tkinter.Checkbutton(window, text="Keep me logged in")
		logged_in_checkbox.grid(columnspan=2)
			
		print_button = tkinter.Button(window, text="Print (my) name", command=self.print_name)
		print_button.grid(columnspan=2)
		
		window.mainloop()


if __name__ == '__main__':
	App()