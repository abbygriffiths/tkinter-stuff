#!/usr/bin/env python3

import tkinter


class App:
    def __init__(self):
        window = tkinter.Tk()
        
        menu = tkinter.Menu(window)
        window.config(menu=menu)
        
        file_menu = tkinter.Menu(menu)
        menu.add_cascade(label="File", menu=file_menu)
        
        edit_menu = tkinter.Menu(menu)
        menu.add_cascade(label="Edit", menu=edit_menu)
        
        file_menu.add_command(label="Do nothing", command=self.do_nothing)
        file_menu.add_command(label="Nothing here either.",
                              command=self.do_nothing)
        file_menu.add_separator()
        file_menu.add_command(label="Exit", command=window.quit)
        
        edit_menu.add_command(label="Absolutely no functionality.",
                              command=self.do_nothing)
        
        # Toolbar
        toolbar = tkinter.Frame(window, bd=1, relief=tkinter.RAISED)
        
        insert_button = tkinter.Button(toolbar,
                                       text="Do Nothing",
                                       command=self.do_nothing)
        insert_button.pack(side=tkinter.LEFT, padx=2, pady=2)
        
        print_button = tkinter.Button(toolbar,
                                      text="What'd you expect",
                                      command=self.do_nothing)
        print_button.pack(side=tkinter.LEFT, padx=2, pady=2)
        
        toolbar.pack(side=tkinter.TOP, fill=tkinter.X)
        
        # Status Bar
        
        status = tkinter.Label(window,
                               text="Preparing to do nothing...",
                               bd=1,
                               relief=tkinter.SUNKEN,
                               anchor=tkinter.W)
        status.pack(side=tkinter.BOTTOM, fill=tkinter.X)
        
        window.mainloop()
        
    
    def do_nothing(self):
        print("Not doing anything...")
        
        
if __name__ == '__main__':
    App()