#!/usr/bin/env python3

import tkinter
from tkinter import constants


class App:
	def __init__(self):
		window = tkinter.Tk()
		
		tkinter.Label(window, text="Hello, World!").pack()
		tkinter.Button(window, text='Exit', command=window.destroy).pack()

		window.mainloop()


if __name__ == '__main__':
	App()
