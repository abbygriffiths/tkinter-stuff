#!/usr/bin/env python3

import tkinter
import tkinter.messagebox


class App:
    def __init__(self):
        window = tkinter.Tk(screenName="App", className="App", baseName="App")
        
        tkinter.messagebox.showinfo("Window Title",
                                    "Money is just paper with value.")
        
        answer = tkinter.messagebox.askquestion("Question 1",
                                                "Do you like silly faces?")
        if answer == 'yes':
            print("8====D~")
        window.mainloop()
        
        
if __name__ == '__main__':
    App()