#!/usr/local/bin/python3


import tkinter
from tkinter import constants


class App:
	def __init__(self):
		window = tkinter.Tk()
		
		one = tkinter.Label(window, text="One", fg='white', bg='red')
		one.pack()
		
		two = tkinter.Label(window, text="Two", fg='black', bg='green')
		two.pack(fill=constants.X)
		
		three = tkinter.Label(window, text="Three", fg='white', bg='blue')
		three.pack(fill=constants.Y, side=constants.LEFT)
		
		window.mainloop()
		
		
if __name__ == '__main__':
	App()